<?php 
	echo '<h2> Tugas OOP </h2>';
	require("animal.php");
	require("frog.php");
	require("ape.php");
	$sheep = new Animal("shaun");
	$sheep->name;
	//echo $sheep->legs;
	//echo $sheep->cold_blooded;

	echo "<br>";

	$kodok = new Frog("buduk");
	$kodok->jump();
	//$kodok = new Frog("buduk");
	//$kodok->jump() ;

	echo "<br>";

	$sungokong = new Ape("kera sakti");
	$sungokong->yell();
	//$sungokong = new Ape("kera sakti");
	//$sungokong->yell() // "Auooo"	
?>